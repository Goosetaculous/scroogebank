#!/bin/bash

declare SQL_VERSION
declare DB_HOST=127.0.0.1
declare DB_USER=root
declare DB_PASSWORD=password
declare DB_DATABASE=scroogebank
declare PORT
declare IMAGE_NAME
declare MYSQL_VERSION

function build_docker_image() {
    echo "================ Building Docker Image ====================="
    echo ""
    echo " Building with database ${DB_DATABASE}"
    docker build --no-cache=true -t "${IMAGE_NAME}" .
    echo "================ Finished Building Docker Image ====================="
}

function docker_run() {
    echo "================ Running Docker Image ====================="
    echo ""
    echo ${PORT}
    Docker run -d --name "${IMAGE_NAME}" --env=MYSQL_ROOT_PASSWORD=password -p ${PORT}:3306 "${IMAGE_NAME}"
    echo "================ It should be Running now ====================="
}

function usage() {
    echo ""
    echo "USAGE:"
    echo ""
    echo "./build.sh {image_name} {port} {database_name}"
    echo " - example: ./build.sh development 3306"
    echo " - will build an MySQL 8.0 a container called development under port 3306"
    echo ""
}

usage

if [ -z $1 ]; then
    echo ""
    echo "Warning you need to specify container name (Ideally it could be related to a story / jira ticket)"
    echo ""
    exit 0
else
    IMAGE_NAME=$1
fi

if [ -z $2 ]; then
    echo ""
    echo "Warning you need to specify the PORT for your container MySql"
    echo ""
    exit 0
else
    PORT=$2
fi

if [ -z $3 ]; then
    echo ""
    echo "Warning you need to specify the name for your container MySql"
    echo ""
    exit 0
else
    DB_DATABASE=$3
fi




build_docker_image
docker_run

# docker exec -it a8a61db6d3bfaf563ce9e2a65ca1c2fe7a4014d1b0367032b2b75b014a40fbf5 /bin/bash
#lower_case_table_names =  1
