/*
 Navicat MySQL Data Transfer

 Source Server         : scrooge
 Source Server Type    : MySQL
 Source Server Version : 80028
 Source Host           : localhost
 Source Database       : scroogedb

 Target Server Type    : MySQL
 Target Server Version : 80028
 File Encoding         : utf-8

 Date: 03/06/2022 16:25:43 PM
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;



-- ----------------------------
--  Table structure for `accounts`
-- ----------------------------
DROP TABLE IF EXISTS `accounts`;
CREATE TABLE `accounts` (
  `id` int NOT NULL AUTO_INCREMENT,
  `guid` varchar(255) NOT NULL,
  `balance` decimal(10,0) unsigned DEFAULT NULL COMMENT 'Balance of the acount',
  `status` enum('open','close') NOT NULL,
  `created_at` timestamp NOT NULL COMMENT 'created at of the account',
  `updated_at` timestamp NOT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT 'updated at of the account',
  PRIMARY KEY (`id`),
  UNIQUE KEY `account_id` (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- ----------------------------
--  Table structure for `deposits`
-- ----------------------------
DROP TABLE IF EXISTS `deposits`;
CREATE TABLE `deposits` (
  `id` int NOT NULL,
  `account_guid` varchar(255) NOT NULL,
  `amount` decimal(10,0) NOT NULL,
  `status` bit(1) NOT NULL,
  `created_at` timestamp NOT NULL,
  `updated_at` timestamp NOT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- ----------------------------
--  Table structure for `loans`
-- ----------------------------
DROP TABLE IF EXISTS `loans`;
CREATE TABLE `loans` (
  `id` int NOT NULL AUTO_INCREMENT,
  `guid` varchar(255) NOT NULL,
  `balance` decimal(10,0) unsigned DEFAULT NULL COMMENT 'Balance of the acount',
  `status` enum('open','close') NOT NULL,
  `created_at` timestamp NOT NULL COMMENT 'created at of the account',
  `updated_at` timestamp NOT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT 'updated at of the account',
  PRIMARY KEY (`id`),
  UNIQUE KEY `account_id` (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- ----------------------------
--  Table structure for `users`
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` int NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) DEFAULT NULL,
  `name` varchar(255) NOT NULL,
  `country` varchar(255) DEFAULT NULL,
  `account_guid` varchar(255) DEFAULT NULL,
  `loan_guid` varchar(255) DEFAULT NULL,
  `created_at` timestamp NOT NULL,
  `updated_at` timestamp NOT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- ----------------------------
--  Table structure for `withdrawals`
-- ----------------------------
DROP TABLE IF EXISTS `withdrawals`;
CREATE TABLE `withdrawals` (
  `id` int NOT NULL,
  `account_guid` varchar(255) NOT NULL,
  `amount` decimal(10,0) NOT NULL,
  `status` bit(1) NOT NULL,
  `created_at` timestamp NOT NULL,
  `updated_at` timestamp NOT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

SET FOREIGN_KEY_CHECKS = 1;
