# Scrooge SYSTEM (BETA)
Banking System for Scroogebank 
## Description
Scrooge Bank has opened for business! We are targeting customers who are technically savvy and have decided they love doing banking all via API Requests.

## Getting Started
1. Run the database
    * Goto the folder DatabaseInstance
    * run ./build.sh file with `./build.sh scoorgedb 3306 scroogedb`
2. Cd to the root folder
    * run the command `npm run dev`
### Dependencies
* Node.js version 17 and up
* Typescript version 4.6.2
* Docker
### Installing
* After downloading the repository do the following/
```
npm install
```
### Executing program
* For Development 
```
npm run dev
```
* For running on local maching
```
npm start
```
* To build the package
```
npm run build
```
* To run on docker
```
docker run -d 3000:3000 {whatever name}
```
# Tools and Platforms
 Gitlab - Friendly repository that has good support of CI/CD pipeline  
 Typescript - strict syntactical superset of JavaScript and adds optional static typing to the language  
 MySQL - Database used that runs in the cloud. (Current choice by the requester)
## Table Definitions
### Accounts
Stores the accounts for a specific user.
Types:
    Bank Account
    Loan Account
    Operator Account

### Deposits
Table containing all deposit transactions.

### Payments
Table containing all payment transactions.

### Users
Table containing all user with the following access_types.
Types:
    operator
    bank
### Withdrawals
Table containing all withdrawal transactions.
# Deliverable Endpoints

POST: http://localhost:3000/user/create - creates a user
### Sample Post
```
{
    "email": "user@gmail.com",
    "name": "Harper",
    "password": "password",
    "accessType": "operator"
}

```


POST: http://localhost:3000/auth/login - retrieves the bearer token
### Sample Post
```
{
    "email": "user@gmail.com",
    "password": "password"
}
```


POST: http://localhost:3000/account/openbankaccount - opens a bank account with 0 balance
### Sample Post
```
Bearer Token: {token}

{
    
    "amount": 1000
}
```



POST: http://localhost:3000/deposit - Deposits to the account
### Sample Post
```
Bearer Token: {token}

{
    
    "amount": 1000
}
```


POST: http://localhost:3000/withdraw - Withdraw from the account
### Sample Post
```
Bearer Token: {token}

{
    
    "amount": 1000
}
```




# Authors
Contributors names and contact info  
Joseph "Goose" Aranez  
ex. [Linkedin](https://www.linkedin.com/in/joseph-goose-aranez/)
# Version History
* 0.1
* Initial commit
* See [commit change]() or See [release history]()
* 0.1.0
* Initial Release
* 0.1.1

# Flow / Interaction


## Some use cases
- Able to close an account
- Able to apply for a loan


# License
This project is licensed under the [NAME HERE] License - see the LICENSE.md file for details
