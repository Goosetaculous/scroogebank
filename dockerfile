FROM node:16.3

WORKDIR /usr/src/app

# COPY ONLY package.json not the package-lock.json
COPY package*.json ./

RUN npm install

COPY . .

EXPOSE 3000

CMD ["npm", "start"]