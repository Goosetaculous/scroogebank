import App from '../../providers/app';
import request from 'supertest';

describe('USER route', () => {
  it('/user/test should return USER_TEST_ROUTE', async () => {
    const result = await request('localhost:3000').get('/user/test');
    expect(result.text).toEqual('USER_TEST_ROUTE');
    expect(result.statusCode).toEqual(200);
  });

  it('/user/create should create a user on the db', async () => {
    const result = await request('localhost:3000')
      .post('/user/create')
      .field('email', 'user3@y1ahoo.com')
      .field('name', 'Harper')
      .field('password', 'password')
      .field('accessType', 'operator')
      .expect((response) => {
        expect(response.statusCode).toEqual(200);

        expect(response.text).toEqual('CREATED');
      });
  });

  it('/auth/login should return a token', async () => {
    // const result = await request('localhost:3000')
    //   .post('/auth/login')
    //   .auth('user3@y1ahoo.com', 'password')
    //   .expect((response) => {
    //     console.log(response);
    //     expect(response.statusCode).toEqual(200);
    //     console.log(response.text);
    //     expect(response.text).toEqual('CREATED');
    //   });
  });
});
