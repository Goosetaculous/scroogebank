import { Accounts } from '../entities/Accounts';
import accountService from '../services/account.service';
import userService from '../services/user.service';

const OPEN: string = 'open';
const CLOSE: string = 'close';

class AccountController {
  private acctService;
  private userService;
  constructor() {
    this.acctService = accountService;
    this.userService = userService;
  }

  /**
   * @function {openAccount}
   * @param  {type} obj: any { User data to be queried }
   * @return {type} {Returns a promise to open an account}
   */
  public async openAccount(data: Accounts): Promise<Accounts> {
    const user_guid: string = data.user;
    const { accessType } = await this.userService.getUserByGuid(user_guid);
    const acct_type: string = accessType;
    data.type = accessType;

    const openAccounts = await this.acctService.checkForOpenAccounts(
      user_guid,
      acct_type
    );
    if (Array.isArray(openAccounts) && !openAccounts.length) {
      const newAccount = await Accounts.save(data);
      return newAccount;
    }
    throw new Error('MUST_CLOSE_OPEN_ACCOUNTS');
  }

  /**
   * @function {openLoan}
   * @param  {type} obj: any { User data to be queried }
   * @return {type} {Returns a promise to open an account}
   */
  public async openLoan(data: Accounts): Promise<Accounts> {
    const user_guid: string = data.user;
    const acct_type: string = data.type;
    const operatorAccount = await this.acctService.getOperatorAccount();
    const isLoanValid: boolean = await this.acctService.validatedLoan(
      operatorAccount,
      data
    );

    if (isLoanValid) {
      const openAccounts = await this.acctService.checkForOpenAccounts(
        user_guid,
        acct_type
      );
      if (Array.isArray(openAccounts) && !openAccounts.length) {
        const newAccount = await Accounts.save(data);
        if (newAccount) {
          data.status = 'open';
          await Accounts.save(data);
        }

        operatorAccount[0].balance =
          parseInt(operatorAccount[0].balance.toString()) - data.balance;
        await Accounts.save(operatorAccount[0]);
        return newAccount;
      }
      throw new Error('MUST_CLOSE_OPEN_ACCOUNTS');
    }

    throw new Error('LOAN_NOT_VALID');
  }

  /**
   * @function {closeAccount}
   * @param  {type} obj: any { User data to be queried }
   * @return {type} {Returns a promise to open an account}
   */
  public async closeAccount(data: Accounts): Promise<Accounts> {
    const openAccounts = await this.acctService.checkForOpenAccounts(data.user);
    if (Array.isArray(openAccounts) && openAccounts.length) {
      openAccounts[0].status = CLOSE;
      return await Accounts.save(openAccounts[0]);
    }
    throw new Error('NO_OPEN_ACCOUNTS');
  }
}

export default new AccountController();
