import { Deposits } from '../entities/Deposits';
import { Accounts } from '../entities/Accounts';
import accountService from '../services/account.service';
import userService from '../services/user.service';
import { Guid } from 'guid-typescript';

const OPEN: string = 'open';
// const CLOSE: string = 'close';
const SUCCESS: string = 'success';
// const FAILED: string = 'failed';

class DepositController {
  private acctService;
  private userService;
  constructor() {
    this.acctService = accountService;
    this.userService = userService;
  }

  /**
   * @function {depositToBank}
   * @param  {type} data: any { User data to be queried }
   * @return {type} {Returns a promise to open an account}
   */
  public async depositToBank(data: any): Promise<Deposits> {
    const user_guid = data.user;
    const { accessType } = await this.userService.getUserByGuid(user_guid);
    const openAccounts = await this.acctService.checkForOpenAccounts(
      user_guid,
      accessType
    );

    if (!Array.isArray(openAccounts)) throw 'INTERNAL_ERROR';
    if (accessType === 'bank' && !openAccounts.length) {
      throw 'NO_OPEN_ACCOUNTS';
    }

    const deposit: any = {
      guid: Guid.create().toString(),
      accountGuid: user_guid,
      amount: data.amount,
      status: SUCCESS,
    };

    openAccounts[0].balance =
      parseInt(openAccounts[0].balance.toString()) + parseInt(deposit.amount);
    await Accounts.save(openAccounts[0]);
    return await Deposits.save(deposit);
  }
}

export default new DepositController();
