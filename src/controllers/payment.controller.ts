import { Payments } from '../entities/Payments';
import { Accounts } from '../entities/Accounts';
import accountService from '../services/account.service';
import { Guid } from 'guid-typescript';

const SUCCESS: string = 'success';

class PaymentController {
  private acctService;
  constructor() {
    this.acctService = accountService;
  }

  /**
   * @function {paymentToLoan}
   * @param  {type} data: any { User data to be queried }
   * @description  {None} { Makes a payment to a loan }
   * @return {type} {Returns a promise to open an account}
   */
  public async paymentToLoan(data: any): Promise<Payments> {
    const openAccounts = await this.acctService.checkForOpenAccounts(
      data.user,
      'loan'
    );

    if (!Array.isArray(openAccounts)) throw 'INTERNAL_ERROR';
    if (!openAccounts.length) throw 'NO_OPEN_LOAN_ACCOUNTS';
    if (parseInt(openAccounts[0].balance.toString()) <= 0)
      throw 'LOAN_IS_PAID_OFF';

    const payment: any = {
      guid: Guid.create().toString(),
      acctGuid: openAccounts[0].guid,
      amount: data.amount,
      status: SUCCESS,
    };

    openAccounts[0].balance =
      parseInt(openAccounts[0].balance.toString()) - parseInt(payment.amount);
    const loanResult = await Accounts.save(openAccounts[0]);
    if (loanResult.balance === 0) {
      loanResult.status = 'close';
      await Accounts.save(loanResult);
    }
    return await Payments.save(payment);
  }
}

export default new PaymentController();
