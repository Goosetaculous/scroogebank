import { Users } from '../entities/Users';
import { BaseEntity, EntityRepository } from 'typeorm';

@EntityRepository(Users)
class UserController extends BaseEntity {
  constructor() {
    super();
  }

  /**
   * @function {createUser}
   * @param  {type} obj: any {User data to be inserted }
   * @return {type} {Returns a promise}
   */
  public async createUser(obj: any): Promise<Users> {
    return Users.save(obj);
  }
}

export default new UserController();
