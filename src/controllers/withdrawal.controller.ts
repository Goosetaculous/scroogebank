import { Withdrawals } from '../entities/Withdrawals';
import { Accounts } from '../entities/Accounts';
import accountList from '../interface/account.interface';
import accountService from '../services/account.service';
import { Guid } from 'guid-typescript';

const OPEN: string = 'open';
// const CLOSE: string = 'close';
const SUCCESS: string = 'success';
// const FAILED: string = 'failed';

class WithdrawalController {
  private acctService;
  constructor() {
    this.acctService = accountService;
  }

  /**
   * @function {depositToAccount}
   * @param  {type} data: any { User data to be queried }
   * @return {type} {Returns a promise to open an account}
   */
  public async withdrawFromAccount(data: any): Promise<Withdrawals> {
    const openAccounts = await this.acctService.checkForOpenAccounts(data.user);

    if (!Array.isArray(openAccounts)) throw 'INTERNAL_ERROR';
    if (!openAccounts.length) throw 'NO_OPEN_ACCOUNTS';

    const withdrawal: any = {
      guid: Guid.create().toString(),
      accountGuid: openAccounts[0].guid,
      amount: data.amount,
      status: SUCCESS,
    };
    if (parseInt(openAccounts[0].balance.toString()) < withdrawal.amount)
      throw new Error('CANT_WITHDRAW_BALANCE_NOT_ENOUGH');

    openAccounts[0].balance =
      parseInt(openAccounts[0].balance.toString()) - withdrawal.amount;
    await Accounts.save(openAccounts[0]);

    return await Withdrawals.save(withdrawal);
  }
}

export default new WithdrawalController();
