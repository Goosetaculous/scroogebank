import {
  BaseEntity,
  Column,
  Entity,
  Index,
  PrimaryGeneratedColumn,
} from 'typeorm';

@Index('account_id', ['id'], { unique: true })
@Entity('accounts', { schema: 'scroogedb' })
export class Accounts extends BaseEntity {
  @PrimaryGeneratedColumn({ type: 'int', name: 'id' })
  id?: number;

  @Column('varchar', { name: 'guid', length: 255 })
  guid!: string;

  @Column('varchar', { name: 'user', nullable: true, length: 255 })
  user!: string;

  @Column('enum', {
    name: 'type',
    enum: ['bank', 'operator', 'loan'],
    default: () => "'client'",
  })
  type!: string;

  @Column('decimal', {
    name: 'balance',
    nullable: true,
    comment: 'Balance of the acount',
    unsigned: true,
    precision: 10,
    scale: 0,
    default: () => "'0'",
  })
  balance!: number;

  @Column('enum', {
    name: 'status',
    nullable: true,
    enum: ['open', 'close', 'failed'],
  })
  status?: string;

  @Column('timestamp', {
    name: 'created_at',
    comment: 'created at of the account',
    default: () => 'CURRENT_TIMESTAMP',
  })
  createdAt!: Date;

  @Column('timestamp', {
    name: 'updated_at',
    comment: 'updated at of the account',
    default: () => 'CURRENT_TIMESTAMP',
  })
  updatedAt!: Date;
}
