import {
  BaseEntity,
  Column,
  Entity,
  Index,
  PrimaryGeneratedColumn,
} from 'typeorm';

@Index('idx_id', ['id'], { unique: true })
@Entity('deposits', { schema: 'scroogedb' })
export class Deposits extends BaseEntity {
  @PrimaryGeneratedColumn({ type: 'int', name: 'id' })
  id?: number;

  @Column('varchar', { name: 'guid', length: 255 })
  guid!: string;

  @Column('varchar', { name: 'account_guid', length: 255 })
  accountGuid!: string;

  @Column('decimal', { name: 'amount', precision: 10, scale: 0 })
  amount!: number;

  @Column('enum', {
    name: 'status',
    enum: ['success', 'failed'],
    default: () => "'failed'",
  })
  status!: string;

  @Column('timestamp', {
    name: 'created_at',
    nullable: true,
    default: () => 'CURRENT_TIMESTAMP',
  })
  createdAt?: Date | null;

  @Column('timestamp', { name: 'updated_at', nullable: true })
  updatedAt?: Date | null;
}
