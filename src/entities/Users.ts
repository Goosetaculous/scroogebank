import {
  BaseEntity,
  Column,
  Entity,
  Index,
  PrimaryGeneratedColumn,
} from 'typeorm';

@Index('idx_email', ['email'], { unique: true })
@Entity('users', { schema: 'scroogedb' })
export class Users extends BaseEntity {
  @PrimaryGeneratedColumn({ type: 'int', name: 'id' })
  id?: number;

  @Column('varchar', { name: 'guid', nullable: true, length: 255 })
  guid!: string | null;

  @Column('enum', {
    name: 'access_type',
    enum: ['bank', 'operator'],
    default: () => "'client'",
  })
  accessType!: 'bank' | 'operator';

  @Column('varchar', { name: 'email', unique: true, length: 255 })
  email!: string;

  @Column('varchar', { name: 'password', nullable: true, length: 255 })
  password!: string;

  @Column('varchar', { name: 'token', nullable: true, length: 255 })
  token!: string;

  @Column('varchar', { name: 'name', length: 255 })
  name?: string;

  @Column('varchar', { name: 'country', nullable: true, length: 255 })
  country!: string | null;

  @Column('timestamp', {
    name: 'created_at',
    nullable: true,
    default: () => 'CURRENT_TIMESTAMP',
  })
  createdAt!: Date | null;

  @Column('timestamp', { name: 'updated_at', nullable: true })
  updatedAt!: Date | null;
}
