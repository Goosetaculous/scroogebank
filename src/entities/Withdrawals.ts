import { BaseEntity, Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

@Entity('withdrawals', { schema: 'scroogedb' })
export class Withdrawals extends BaseEntity {
  @PrimaryGeneratedColumn({ type: 'int', name: 'id' })
  id?: number;

  @Column('varchar', { name: 'guid', length: 255 })
  guid!: string;

  @Column('varchar', { name: 'account_guid', length: 255 })
  accountGuid!: string;

  @Column('decimal', { name: 'amount', precision: 10, scale: 0 })
  amount!: string;

  @Column('bit', { name: 'status' })
  status!: boolean;

  @Column('timestamp', {
    name: 'created_at',
    default: () => 'CURRENT_TIMESTAMP',
  })
  createdAt?: Date;

  @Column('timestamp', { name: 'updated_at' })
  updatedAt?: Date;
}
