import { Accounts } from './Accounts';
import { Deposits } from './Deposits';
import { Payments } from './Payments';
import { Users } from './Users';
import { Withdrawals } from './Withdrawals';

export default [Accounts, Deposits, Payments, Users, Withdrawals];
