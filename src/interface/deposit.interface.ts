export default interface depositSlip {
  user_guid: string | undefined;
  amount: number;
}
