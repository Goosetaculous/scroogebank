export interface userInterface {
  userId: string;
  firstName: string;
  lastName: string;
  username: string;
  balance: number;
}
