// EXPRESS
import Express from './express';
// Database
import ORM from './database';

// REquired for typeorm
// import 'reflect-metadata';

class App {
  constructor() {}

  public loadServer(): void {
    Express.init();
  }
  public connectDB(): void {
    console.log(`DATABASE connected ? ${ORM.isdbconnected()}`);
  }
}

export default new App();
