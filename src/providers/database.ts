import { getConnection, createConnections } from 'typeorm';

class ORM {
  constructor() {
    this.loadDatabase();
  }

  private loadDatabase(): void {
    try {
      createConnections([
        {
          name: 'default',
          type: 'mysql',
          host: 'localhost',
          port: 3306,
          username: 'root',
          password: 'password',
          database: 'scroogedb',
          // synchronize: true,
          //logging: true,
          ssl: true,
          extra: {
            ssl: {
              rejectUnauthorized: false,
            },
          },
          entities: ['dist/entities/*.{js,ts}'],
          // entities: entities.default,
        },
      ]);
    } catch {
      console.log('ERROR');
    }
  }

  public isdbconnected(): boolean {
    try {
      const connection = getConnection();
      if (connection) return true;
    } catch (error) {
      return false;
    }
    return false;
  }
}

export default new ORM();
