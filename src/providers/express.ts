// EXPRESS
import express from 'express';
import compression from 'compression';
import cors from 'cors';
import helmet from 'helmet';
import 'reflect-metadata';
import fs = require('fs');
import swaggerUi = require('swagger-ui-express');
// Routes
import { User } from '../routes/user.route';
import { Auth } from '../routes/auth.route';
import { Account } from '../routes/account.route';
import { Deposit } from '../routes/deposit.route';
import { Withdrawal } from '../routes/withdrawal.route';
import { Payment } from '../routes/payment.route';

// Middlware
import { Middleware } from '../services/middleware';

const swaggerDocument = require('../../swagger.json');

class Express extends Middleware {
  private app: express.Application;
  private userRoutes: User = new User();
  private authRoutes: Auth = new Auth();
  private acctRoutes: Account = new Account();
  private depositRoutes: Deposit = new Deposit();
  private withdrawRoutes: Withdrawal = new Withdrawal();
  private paymentRoutes: Payment = new Payment();

  constructor() {
    super();
    this.app = express();
  }

  /**
   * @function {Load the routes}
   * @return {type} {None}
   */
  private loadMainRoutes(): void {
    this.userRoutes.routes(this.app);
    this.authRoutes.routes(this.app);
    this.acctRoutes.routes(this.app);
    this.depositRoutes.routes(this.app);
    this.withdrawRoutes.routes(this.app);
    this.paymentRoutes.routes(this.app);
  }
  /**
   * @function {Initialize the Express Configurations}
   * @return {type} {None}
   */
  private applyConfig(): any {
    this.app.use(express.json());
    this.app.use(express.urlencoded({ extended: false }));
    this.app.use(cors());
    this.app.use(compression());
    this.app.use(helmet());
    this.app.use(this.logging);
    this.loadMainRoutes();
    this.app.use(this.errorHandler);
    this.app.use(
      '/api-docs',
      swaggerUi.serve,
      swaggerUi.setup(swaggerDocument)
    );
  }
  /**
   * @function {init}
   * @return {type} {Initialize the web server}
   */
  public init(): any {
    const PORT = process.env.PORT || 3000;
    (async () => {
      const app = this.app;
      this.applyConfig();
      app.listen(PORT, () => console.log('Server online'));
    })();
  }
}

export default new Express();
