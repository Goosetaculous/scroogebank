import { NextFunction, Request, Response } from 'express';
import AccountController from '../controllers/account.controller';
import { Middleware } from '../services/middleware';
import { Guid } from 'guid-typescript';

const OPEN: string = 'open';
const CLOSE: string = 'close';
export class Account {
  private resource: string = '/account';
  private middleware: Middleware = new Middleware();

  public routes(app: any): void {
    app
      .route(this.resource + '/openbankaccount')
      .post(
        this.middleware.authenticatedRoute,
        async (req: Request, res: Response, next: NextFunction) => {
          try {
            let user_guid = req.user;

            let data: any = {
              guid: Guid.create().toString(),
              user: user_guid,
              balance: 0.0,
            };
            await AccountController.openAccount(data);
            res.status(200).send('ACCOUNT_OPENED');
          } catch (error) {
            next('MUST_CLOSE_SOME_ACCOUNTS');
          }
        }
      );

    app
      .route(this.resource + '/applyforloan')
      .post(
        this.middleware.authenticatedRoute,
        async (req: Request, res: Response, next: NextFunction) => {
          try {
            let user_guid = req.user;
            let requestedAmount = req.body.amount;
            const type = 'loan';
            let data: any = {
              guid: Guid.create().toString(),
              user: user_guid,
              balance: requestedAmount,
              type,
            };
            await AccountController.openLoan(data);
            res.status(200).send('LOAN_OPENED');
          } catch (error) {
            next(error);
          }
        }
      );

    app
      .route(this.resource + '/close')
      .post(
        this.middleware.authenticatedRoute,
        async (req: Request, res: Response, next: NextFunction) => {
          try {
            let user_guid = req.user;
            let data: any = {
              user: user_guid,
              status: OPEN,
            };
            await AccountController.closeAccount(data);

            res.status(200).send('ACCOUNT_CLOSED');
          } catch (error) {
            next(error);
          }
        }
      );
  }
}
