import { NextFunction, Request, Response } from 'express';
import { Authentication } from '../services/authentication';

export class Auth {
  private resource: string = '/auth';

  public routes(app: any): void {
    /**
     * @Route {Route login}
     * @description  {type} {Verifies the email and password}
     * @return {type} {If authenticated, returns a token to be used on bearer}
     */
    app
      .route(this.resource + '/login')
      .post(async (req: Request, res: Response, next: NextFunction) => {
        try {
          const email = req.body.email;
          const password = req.body.password;
          const USER = new Authentication(email, password);
          const token = await USER.verifyPassword();

          res.status(200).send(token);
        } catch (error) {
          next(error);
        }
      });

    // TEST ROUTE
    app
      .route(this.resource + '/test')
      .get((_: Request, res: Response, next: NextFunction) => {
        try {
          res.status(200).send('CREATED');
        } catch (error) {
          next(error);
        }
      });
  }
}
