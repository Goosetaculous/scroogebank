import { NextFunction, Request, Response } from 'express';
import DepositController from '../controllers/deposit.controller';
import { Middleware } from '../services/middleware';

export class Deposit {
  private resource: string = '/deposit';
  private middleware: Middleware = new Middleware();

  public routes(app: any): void {
    app
      .route(this.resource)
      .post(
        this.middleware.authenticatedRoute,
        async (req: Request, res: Response, next: NextFunction) => {
          try {
            let user_guid = req.user;
            let data: any = {
              user: user_guid,
              amount: req.body.amount,
            };
            await DepositController.depositToBank(data);
            res.status(200).send('DEPOSITED');
          } catch (error) {
            next(error);
          }
        }
      );

    // TEST ROUTE
    app.route(this.resource + '/test').get(
      // this.middleware.authenticatedRoute,
      (req: Request, res: Response, next: NextFunction) => {
        try {
          res.status(200).send('DEPOSIT_VERIFIED');
        } catch (error) {
          next(error);
        }
      }
    );
  }
}
