import { NextFunction, Request, Response } from 'express';
import paymentController from '../controllers/payment.controller';
import { Middleware } from '../services/middleware';

export class Payment {
  private resource: string = '/loan';
  private middleware: Middleware = new Middleware();

  public routes(app: any): void {
    app
      .route(this.resource + '/payment')
      .post(
        this.middleware.authenticatedRoute,
        async (req: Request, res: Response, next: NextFunction) => {
          try {
            let user_guid = req.user;
            let data: any = {
              user: user_guid,
              amount: req.body.amount,
            };
            await paymentController.paymentToLoan(data);
            res.status(200).send('PAYMENT_SUCCEEDED');
          } catch (error) {
            next(error);
          }
        }
      );

    app
      .route(this.resource + '/loanpayment')
      .post(async (req: Request, res: Response, next: NextFunction) => {
        try {
          // GRAB THE USER GUID
          // VERIFY ITS ONE TO ONE
          // INSERT IT WITH GUID
          res.status(200).send('CREATED');
        } catch (error) {
          next(error);
        }
      });

    // TEST ROUTE
    app.route(this.resource + '/test').get(
      // this.middleware.authenticatedRoute,
      (req: Request, res: Response, next: NextFunction) => {
        try {
          res.status(200).send('DEPOSIT_VERIFIED');
        } catch (error) {
          next(error);
        }
      }
    );
  }
}
