import { NextFunction, Request, Response } from 'express';
import UserController from '../controllers/user.controller';
import { Authentication } from '../services/authentication';
import { Guid } from 'guid-typescript';

export class User {
  private resource: string = '/user';

  public routes(app: any): void {
    app
      .route(this.resource + '/create')
      .post(async (req: Request, res: Response, next: NextFunction) => {
        try {
          const email = req.body.email;
          const password = req.body.password;
          const guid = Guid.create().toString(); // Generate UID
          const auth = new Authentication(email, password, guid);

          req.body.password = await auth.hashPassword();
          req.body.token = auth.createToken(guid);
          req.body.guid = guid;
          await UserController.createUser(req.body);
          res.status(200).send('CREATED');
        } catch (error) {
          console.log(error);
          next('FAILED_TO_CREATE_USER');
        }
      });

    // TEST ROUTE
    app
      .route(this.resource + '/test')
      .get((req: Request, res: Response, next: NextFunction) => {
        try {
          res.status(200).send('USER_TEST_ROUTE');
        } catch (error) {
          next(error);
        }
      });
  }
}
