import { NextFunction, Request, Response } from 'express';
import WithdrawalController from '../controllers/withdrawal.controller';
import { Authentication } from '../services/authentication';
import { Middleware } from '../services/middleware';

export class Withdrawal {
  private resource: string = '/withdraw';
  private middleware: Middleware = new Middleware();

  public routes(app: any): void {
    app
      .route(this.resource)
      .post(
        this.middleware.authenticatedRoute,
        async (req: Request, res: Response, next: NextFunction) => {
          try {
            // GRAB THE USER GUID
            // GRAB THE OPEN ACCOUNT
            // CHECK IF THERE IS BALANCE
            // UPDATE THE OPEN ACCOUNT
            // INSERT THE WITHDRAWAL

            let user_guid = req.user;
            let data: any = {
              user: user_guid,
              amount: req.body.amount,
            };
            WithdrawalController.withdrawFromAccount(data);
            // VERIFY THERES A BALANCE
            // INSERT IT WITH GUID
            res.status(200).send('WITHDRAWAL');
          } catch (error) {
            next(error);
          }
        }
      );

    // TEST ROUTE
    app
      .route(this.resource + '/test')
      .get(
        this.middleware.authenticatedRoute,
        (req: Request, res: Response, next: NextFunction) => {
          try {
            res.status(200).send('VERIFIED');
          } catch (error) {
            next(error);
          }
        }
      );
  }
}
