import { Accounts } from '../entities/Accounts';
import accountList from '../interface/account.interface';
const OPEN: string = 'open';

class AccountService {
  /**
   * @function {checkForOpenAccounts}
   * @param  {string} user:  { GUID of the user }
   * @return {type} {Returns a promise list of open accounts}
   */
  public async checkForOpenAccounts(
    user: string,
    accountType?: string
  ): Promise<accountList> {
    let conditions: any = {
      user,
      status: OPEN,
    };
    if (['bank', 'loan', 'operator'].includes(accountType!)) {
      conditions.type = accountType;
    }
    const accounts: accountList = await Accounts.find({
      where: conditions,
    });
    return accounts;
  }

  /**
   * @function {checkForOpenAccounts}
   * @param  {string} user:  { GUID of the user }
   * @return {type} {Returns a promise list of open accounts}
   */
  public async getOperatorAccount(): Promise<accountList> {
    let conditions: any = {
      type: 'operator',
      status: OPEN,
    };
    const accounts: accountList = await Accounts.find({
      where: conditions,
    });
    return accounts;
  }

  /**
   * @function {validatedLoan}
   * @return {Boolean} { Returns a boolean if the loan can be processed}
   */

  public async validatedLoan(
    operatorAccount: accountList,
    data: any
  ): Promise<boolean> {
    if (Array.isArray(operatorAccount) && operatorAccount.length) {
      if (parseInt(operatorAccount[0].balance.toString()) < data.balance) {
        data.status = 'failed';
        await Accounts.save(data);
        return false;
      }
      return true;
    }
    return false;
  }
}

export default new AccountService();
