import * as bcrypt from 'bcryptjs';
import * as jwt from 'jsonwebtoken';
import { Users } from '../entities/Users';
import { User } from '../routes/user.route';

export class Authentication {
  private USEREMAIL: string;
  private USERPASSWORD: string;
  private GUID: string;

  constructor(email?: string, password?: string, guid?: string) {
    this.USEREMAIL = email || '';
    this.USERPASSWORD = password || '';
    this.GUID = guid || '';
  }
  /**
   * @function {verifyPassword}
   * @param  {type} password: string {User input}
   * @param  {type} hash: string     {Db hashed}
   * @return {Promise} {Boolean if it matches otherwise error}
   */
  public verifyPassword(): Promise<string> {
    return new Promise(async (resolve, reject) => {
      try {
        const USER = await this.getUserByEmail(this.USEREMAIL);
        console.log(USER);
        if (!USER) return 401; // NO EMAIL FOUND
        const password = USER?.password;
        const match = await bcrypt.compare(this.USERPASSWORD, password);
        if (match) {
          const NEW_TOKEN = this.createToken(USER.guid);
          this.updateTokenWithId(NEW_TOKEN, USER);
          resolve(USER.token);
        }

        resolve('UN_AUTHORIZED');
      } catch (err) {
        reject('ERROR');
      }
    });
  }

  /**
   * @function {createToken}
   * @param  {object} data: any {Any data to be tokenized by JWT}
   * @return {string} {JWT Token}
   */
  public createToken(data: any): string {
    const token: string = jwt.sign(
      {
        data,
      },
      'secret',
      { expiresIn: '1h' }
    );
    return token;
  }

  /**
   * @function {hashPassword}
   * @return {string} {Hashed password by bcrypt}
   */
  public hashPassword(): Promise<string> {
    return bcrypt.hash(this.USERPASSWORD, 10);
  }

  /**
   * @function {verifyToken}
   * @param  {type} token: string {Token to be verified}
   * @return {type} {Verify by secret or by timestamp. then check if id exist on db }
   */
  public async verifyToken(token: string) {
    try {
      const isVerified = jwt.verify(token, 'secret');
      const decodedToken: any = jwt.decode(token);
      if (!(await this.isIdExist(decodedToken.data))) {
        throw 401;
      }
      return isVerified;
    } catch (err) {
      throw err;
    }
  }
  /**
   * @function {isIdExist}
   * @param  {type} id: string {Id to be check}
   * @return {boolean} {Query the db if id exist}
   */
  private async isIdExist(guid: string): Promise<boolean> {
    const user = await Users.findOne({ where: { guid } });
    return user ? true : false;
  }

  /**
   * @function {getUserByEmail}
   * @return {type} {Get the user by}
   */
  public async getUserByEmail(email: string): Promise<Users | undefined> {
    return await Users.findOne({ email });
  }

  /**
   * @function {updateTokenWithId}
   * @param {type} data: query result
   * @param {type} id: id from the mongoose query after created
   * @return {promise} :Run the findOneandupdate.
   * @description: We want the token to include the _id and not include the password
   */
  private async updateTokenWithId(token: string, User: Users): Promise<any> {
    User.token = token;
    return await Users.save(User);
  }
}
