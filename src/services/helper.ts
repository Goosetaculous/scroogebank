import { Guid } from 'guid-typescript';

export default class Helper {
  constructor() {}

  /**
   * @function {Guid G}
   * @return {String} {returns a guid string}
   */
  public guidGenerator(): string {
    return Guid.create().toString();
  }
}
