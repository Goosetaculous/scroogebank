import { Request, Response, NextFunction, Errback } from 'express';
import { Authentication } from './authentication';
import userService from './user.service';

const NAMESPACE = 'middleware';
/**
 * @Class {Middleware}
 * @param  {type}  {Collection of Middlewares for Express}
 */
export class Middleware {
  /**
   * Logs the path of the route called
   */
  logging(request: Request, response: Response, next: NextFunction): void {
    console.log(`${NAMESPACE} ${request.method} ${request.path}`);
    next();
  }

  /**
   * Error Handler to capture anything inside globally to express
   * @param err
   * @param req
   * @param res
   * @param next
   */
  errorHandler(err: Errback, req: Request, res: Response, next: NextFunction) {
    res.status(500).send({ error: err });
  }

  /**
   * @function {checkRoute}
   * @param  {type} req: Request  {description}
   * @param  {type} res: Response {description}
   * @param  {type} next: any     {description}
   * @return {type} {Verify the signature then decode then check on db}
   */
  public authenticatedRoute(req: Request, res: Response, next: any): void {
    const header = req.headers.authorization;
    const USER = new Authentication();
    console.log('Header');
    if (typeof header !== 'undefined') {
      const bearer = header.split(' ');
      const token = bearer[1];
      USER.verifyToken(token)
        .then((decodedToken: any): any => {
          req.user = decodedToken.data;
          next();
        })
        .catch((err) => {
          console.log(err);
          res.status(403).send({ err, code: 'TOKEN_UNVERIFIED' });
        });
    } else {
      //If header is undefined return Forbidden (403)
      res.sendStatus(403);
    }
  }
}
