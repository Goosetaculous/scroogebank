import { Users } from '../entities/Users';

class UserService {
  /**
   * @function {checkForOpenAccounts}
   * @param  {string} user:  { GUID of the user }
   * @return {type} {Returns a user}
   */
  public async getUserByGuid(user: string): Promise<Users> {
    return await Users.findOneOrFail({
      where: {
        guid: user,
      },
    });
  }

  /**
   * @function {checkForOpenAccounts}
   * @param  {string} user:  { GUID of the user }
   * @return {type} {Returns a user}
   */
  public async SaveUserByGuid(user: Users): Promise<Users> {
    return await Users.save(user);
  }
}

export default new UserService();
